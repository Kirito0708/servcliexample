﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ServCli
{
    public class Server
    {
        public IPAddress ipa { get; set; }
        public int port { get; set; }
        public int maxConnection { get; set; }

        public string ServerName { get; set; }
        public string motd { get { return "Welcome to " + ServerName; } }
        
        private Socket server;

        public Server(string ipa, int port, string ServerName, int maxConnection) 
        {
            this.ipa = IPAddress.Parse(ipa);
            this.port = port;
            this.ServerName = ServerName;
            this.maxConnection = maxConnection;
        }

        public void Start() 
        {
            server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPEndPoint ipe = new IPEndPoint(ipa, port);

            Console.WriteLine("Attempting to bind socket..");
            try 
            {
                server.Bind(ipe);
            } 
            catch (SocketException se) 
            {
                Console.WriteLine("Failed to bind Socket! : ");
                Console.WriteLine(se.Message);
                server.Close();
                Console.ReadKey();
            }
            Console.WriteLine("Socket sucessfully binded to " + ipe.Address.ToString() + ":" + ipe.Port.ToString());    

            server.Listen(maxConnection);

            Socket client = server.Accept();
            Console.WriteLine("User connected from " + client.LocalEndPoint.ToString());
            string data = motd;

            byte[] tobesent = Encoding.ASCII.GetBytes(motd);
            client.Send(tobesent);
            client.Close();
            server.Close();
            Console.ReadKey();
        }
    }
}
